require('keys/alias')

local bufopts = { noremap = true, silent = true, buffer = bufnr }

function nnoremap(rhs, lhs, bufopts, desc)
	bufopts.desc = desc
	vim.keymap.set("n", rhs, lhs, bufopts)
end

-- nm("<space>ar",":tabnew | term sh ./easy_run.sh %<CR>'")
nm("<space>ar",":tabnew | term sh easy_run.sh " .. vim.fn.expand('%:p') .."<CR>a")
