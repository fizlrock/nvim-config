require('keys/alias')

local map = vim.api.nvim_set_keymap

im('<C-k>', '<escape>') -- Дополнительная комбинация для возвращение в нормальный режим


-- Удобное сохранение и выход
nm('Q', ':q<CR>')
nm('S', ':w<CR>')

-- Базовые клавиши перемещения

map('', 'u', 'k', { noremap = true })
map('', 'n', 'h', { noremap = true })
map('', 'e', 'j', { noremap = true })
map('', 'i', 'l', { noremap = true })

map('', 'U', '5k', { noremap = true })
map('', 'E', '5j', { noremap = true })
map('', 'N', '0', { noremap = true })
map('', 'I', '$', { noremap = true })



-- Редактирование

nm('l', 'u') -- Операция отмены
nm('k', 'i') -- Вставка слева от курсора
nm('K', 'I') -- Вставка в начале строки


-- Работа со вкладками


map('', 'tu', ':tabe<CR>', { noremap = true })
map('', 'tU', ':tab split<CR>', { noremap = true })
map('', 'tn', ':-tabnext<CR>', { noremap = true })
map('', 'ti', ':+tabnext<CR>', { noremap = true })




-- LSP
local bufopts = { noremap = true, silent = true, buffer = bufnr }
vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
vim.keymap.set('n', '<space>wl', function()
	print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end, bufopts)
vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)






-- Telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})


-- Neovim Tree
nm('<leader>b',':NvimTreeToggle<CR>')


-- Obsidian


nm('<space>os',':ObsidianSearch<CR>')
nm('<space>oq',':ObsidianQuickSearch<CR>')
nm('<space>ot',':ObsidianToday<CR>')
nm('<space>oy',':ObsidianYesterday<CR>')


-- Trouble
nm('<leader>t', ':TroubleToggle<CR>')



