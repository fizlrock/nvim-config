local nvim_lsp = require('lspconfig')
local servers = { "omnisharp", "bufls", "pyright",
	'dotls', 'bashls', 'gopls', 'lua_ls' }

local on_attach = function(client, bufnr)
	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Mappings.
	-- See `:help vim.lsp.*` for documentation on any of the below functions
end

for _, lsp in ipairs(servers) do
	nvim_lsp[lsp].setup
	{
		snippet = { expand = function() end },
		on_attach = on_attach
	}
end


nvim_lsp.omnisharp.setup
{
	cmd = {"omnisharp"}
}



require 'lspconfig'.lemminx.setup {
	settings = {
		['fileAssociations fuck suck'] = {
			{
				systemId = '~/temp/AvaloniaSchema-11.0.4.xsd',
				pattern = '.axaml'
			}
		}

	}
}

require 'lspconfig'.ccls.setup {
	init_options = {
		compilationDatabaseDirectory = "build",
		index = {
			threads = 0,
		},
		clang = {
			excludeArgs = { "-frounding-math" },
		},
	}
}
